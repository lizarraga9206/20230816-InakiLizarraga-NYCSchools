# Project Title

NYCSchools code assessment

## Getting Started

This project is a simple app that retrieves the high schools from New York City.
1. It has the capability to search by ZIP code so user can enter the actual zip code they live or any other zip code.
2. It will retrieve a full list with 100s of high schools and when you select one it will get you to a more detail overview of the school as well as the SAT scores.

## Built With

- The UI was built using Jetpack compose
- The architecture followed was MVVM + clean architecture
- Network connection was made by using Retrofit and OkHttp with force cache implemented
- The depednecy injection framework used was Hilt from Jetpack
- Asychronous operations were made in the app using Kotlin coroutines and Kotlin flows
- For the unit testing libraries such as mockK, Truth assertions and Junit were used

## Future roadmap

- Functionality to retrieve the current location automatically and display schools based on it
- Favourites feature to save the schools you would liek to apply or the one you like the most

## Authors

  - Inaki Lizarraga -
